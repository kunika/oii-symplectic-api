<?php
	
	// Application settings
	define( 'DEBUG', true );
	
	// Database settings
	define( 'DB_HOST', 'localhost' );
	define( 'DB_USER', 'api' );
	define( 'DB_PASSWORD', 'password' );
	define( 'DB_NAME', 'oii_symplectic' );
	
	// Symplectic API settings
	define( 'SYMPLECTIC_ENDPOINT', 'https://oxris.ox.ac.uk:8091/elements-api/v4.9' );
	define( 'SYMPLECTIC_USER', 'symplecticOII' );
	define( 'SYMPLECTIC_PASS', 'pnT64o74' );
	define( 'SYMPLECTIC_CACHE_PATH', './cache' );
	define( 'SYMPLECTIC_CACHE_TIME', 3600 );
	define( 'SYMPLECTIC_GROUP_ID', 219 );
	
	date_default_timezone_set( 'Europe/London' );
	
?>