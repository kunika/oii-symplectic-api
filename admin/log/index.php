<?php
	
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
	
	require_once( '../../_config.php' );
	
	$db_log = new Database();
	$db_log->prepare('
		SELECT * 
		FROM `log`
		ORDER BY `timestamp` DESC;
	');
	$log = $db_log->resultset();
	$db_log->close();
	
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>OII Symplectic</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		<style>
			pre {
				white-space: pre-wrap;       /* css-3 */
				white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
				white-space: -pre-wrap;      /* Opera 4-6 */
				white-space: -o-pre-wrap;    /* Opera 7 */
				word-wrap: break-word;
				font-size: 12px;
			}
			#log pre {
				padding: 0;
				margin: 0;
				border: none;
				background: none;
			}
		</style>	
	</head>
	<body>
		<div class="container">
			<h1>Log</h1>
			<table id="log" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Timestamp</th>
						<th>Type</th>
						<th>Description</th>
						<th>Tags</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach( $log as $entry ): ?>
					<tr>
						<td><?php echo date_format( date_create( $entry['timestamp'] ), 'Y-m-d H:i:s' ); ?></td>
						<td><?php echo $entry['type']; ?></td>
						<td><pre><?php echo $entry['description']; ?></pre></td>
						<td><?php echo $entry['tags']; ?></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>		
		<script   src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</body>
</html>